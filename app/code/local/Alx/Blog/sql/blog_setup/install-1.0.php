<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    CREATE TABLE IF NOT EXISTS `" . $this->getTable('alx_blog') . "`(
        `post_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `create_date` timestamp NULL DEFAULT NULL,
        `modified_date` timestamp NULL DEFAULT NULL,
        `title`  varchar(256)  NULL DEFAULT NULL,
        `description` text  NULL DEFAULT NULL,
        `image` varchar(512)  NULL DEFAULT NULL,
        UNIQUE KEY (`post_id`),
        KEY (`create_date`),
        KEY (`modified_date`)
    ) Engine=InnoDB charset=utf8;
");

$installer->endSetup();
