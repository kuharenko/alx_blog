<?php

class Alx_Blog_Block_Adminhtml_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {



        $data = Mage::getSingleton('adminhtml/session')->getData('form_data');
        Mage::getSingleton('adminhtml/session')->setData('form_data', null);

        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data',
        ));
 
        $form->setUseContainer(true);
 
        $this->setForm($form);
 
        $fieldset = $form->addFieldset('post_form', array(
             'legend' =>Mage::helper('alx_blog')->__('Post Information')
        ));

        $fieldset->addField('title', 'text', array(
             'label'     => Mage::helper('alx_blog')->__('title'),
             'class'     => 'required-entry',
             'required'  => true,
             'name'      => 'title',
             'note'      => Mage::helper('alx_blog')->__('Title'),
        ));

        $fieldset->addField('description', 'editor', array(
            'label'     => Mage::helper('alx_blog')->__('Description'),
            //'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'description',
            'note'      => Mage::helper('alx_blog')->__('Description'),
            'style'     => 'height:15em',
            'wysiwyg'   => true,
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
        ));

        $fieldset->addField('image', 'image', array(
            'label'     => Mage::helper('alx_blog')->__('image'),
            'required'  => false,
            'name'      => 'image',
            'note'      => Mage::helper('alx_blog')->__('image')
        ));

        $form->setValues($data);

        return parent::_prepareForm();
    }
}