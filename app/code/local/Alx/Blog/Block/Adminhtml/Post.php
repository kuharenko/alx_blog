<?php

class Alx_Blog_Block_Adminhtml_Post extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'alx_blog';
        $this->_controller = 'adminhtml_post';
        $this->_headerText = Mage::helper('alx_blog')->__('Post');

        parent::__construct();
        $this->_removeButton('add');

        $this->_addButton('button1', array(
            'label'      => Mage::helper('alx_blog')->__('add'),
            'onclick'    => 'setLocation(\'' . $this->getUrl('blog_admin/adminhtml_post/new') . '\')',
            'class'      => 'add'
        ));
    }
}