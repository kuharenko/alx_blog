<?php

class Alx_Blog_Block_Adminhtml_Post_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('alx_blog_grid');
        $this->setDefaultSort('post_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);


    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('blog/blog_collection');
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }



    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('tax_calculation_rate_id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/delete', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));

        return $this;
    }



    protected function _prepareColumns()
    {
        $helper = Mage::helper('alx_blog');


        $this->addColumn('post_id', array(
            'header' => $helper->__('ID'),
            'index'  => 'post_id',
            'width' => '50px',
        ));


        $this->addColumn('create_date', array(
            'header' => $helper->__('Create date'),
            'index'  => 'create_date',
            'width' => '100px',
            'type'      => 'date'
        ));

        $this->addColumn('modified_date', array(
            'header' => $helper->__('Modified date'),
            'index'  => 'modified_date',
            'width' => '100px',
            'type'      => 'date'
        ));



        $this->addColumn('image', array(
            'header' => $helper->__('image'),
            'index'  => 'image',
            'width' => '150px',
            'renderer'  => 'Alx_Blog_Block_Adminhtml_Renderer_Image',
        ));

        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index'  => 'title'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/index', array('_current'=>true));
    }


    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
                'store'=>$this->getRequest()->getParam('store'),
                'id'=>$row->getId())
        );
    }



}