<?php

class Alx_Blog_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        if ($row->getData('image') != '') {
            return '<img width="100" src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/blog/' . $row->getData('image') . '" />';
        } else {
            return '';
        }

    }
}