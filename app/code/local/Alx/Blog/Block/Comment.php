<?php

class Alx_Blog_Block_Comment extends Mage_Core_Block_Template
{
    protected $post;
    protected $modified_date;
    protected $title;
    protected $description;
    protected $image;

    public function __construct()
    {
        parent::__construct();
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $id = Mage::app()->getRequest()->getParam('id');
        $this->post = Mage::getModel('blog/blog')->load($id);

        if (is_object($this->post)) {
            $this->modified_date = $this->post->getData('modified_date');
            $this->title = $this->post->getData('title');
            $this->description = $this->post->getData('description');
            $this->image = $this->post->getData('image');
        }

        return $this;
    }

    public function getModifiedDate()
    {
        return $this->modified_date;

    }
    public function getTitle()
    {
        return $this->title;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getImage()
    {
        return $this->image;
    }


}