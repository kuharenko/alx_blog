<?php


class Alx_Blog_Block_Pagination extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $collection = Mage::getModel('blog/blog')->getCollection();
        $this->setCollection($collection);

        $toolbar = $this->getLayout()->createBlock('alx_blog/toolbar');

        //this is where you set what options would you like to  have for sorting your grid. Key is your column in your database, value is just value that will be shown in template
        $toolbar->setAvailableOrders(array('created_at'=> 'Created Time','post_id'=>'ID'));
        $toolbar->setDefaultOrder('post_id');
        $toolbar->setDefaultDirection("asc");
        $toolbar->setCollection($collection);
        $this->setChild('toolbar', $toolbar);
        $this->getCollection()->load();
        return $this;
    }

    //this is what you call in your .phtml file to render toolbar
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
}