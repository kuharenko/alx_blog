<?php

class Alx_Blog_Model_Resource_Blog extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('blog/blog', 'post_id');
        $this->_isPkAutoIncrement = false;
    }
}