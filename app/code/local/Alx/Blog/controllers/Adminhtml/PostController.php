<?php

class Alx_Blog_Adminhtml_PostController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }


    protected function _initAction()
    {
        $this->_title($this->__('Blog'))->_title($this->__('Posts'));
        $this->loadLayout();
        $this->_setActiveMenu('Blog/Blog');
        $this->_addContent($this->getLayout()->createBlock('alx_blog/adminhtml_post'));

        return $this;
    }


    public function editAction()
    {
        $this->_initAction();

        $model = Mage::getModel('blog/blog');
        Mage::registry('blog_blog', $model);

        $id = Mage::app()->getRequest()->getParam('id');
        if ($id) {
            $post = Mage::getModel('blog/blog')->load($id);
            $data = $post->getData();
            if (!empty($data['image'])) {
                $data['image'] = 'blog/' . $data['image'];
            }

            Mage::getSingleton('adminhtml/session')->setFormData($data);
        }

        $this->loadLayout();

        $this->_addContent(
            $this->getLayout()->createBlock('alx_blog/adminhtml_edit')
        );
        $this->renderLayout();
    }


    public function deleteAction()
    {
        $ids = Mage::app()->getRequest()->getParam('id');

        if (!is_array($ids)) {
            $ids = array($ids);
        }

        $post = Mage::getModel('blog/blog');

        foreach ($ids as $id) {
            $item = $post->load($id);
            if (is_object($item)) {
                $image = $item->getData('image');
                $path = Mage::getBaseDir('media') . '/blog/' . $image;
                if (is_file($path)) {
                    unlink($path);
                }
                $item->delete();
            }
        }

        Mage::getSingleton('core/session')->addSuccess(Mage::helper('alx_blog')->__('Post successfully deleted'));
        $this->_redirect('*/*');
    }



    public function saveAction()
    {
        $formKey = $this->getRequest()->getParam('form_key');
        $id = (int)$this->getRequest()->getParam('id');
        $title = $this->getRequest()->getParam('title');
        $description = $this->getRequest()->getParam('description');
        $image = $this->getRequest()->getParam('image');

        if ($formKey != Mage::getSingleton('core/session')->getFormKey()){
            die('Form error.');
        }

        try {

            if ($_FILES['image']['error'] < 1) {
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . '/blog/';
                $uploader->save($path, $_FILES['image']['name']);
                $imageName = $_FILES['image']['name'];
            }

            if ($id > 0) {
                $post = Mage::getModel('blog/blog')->load($id);
                $post->setData('modified_date', time());
            } else {
                $post = Mage::getModel('blog/blog');
                $post->setData('create_date', time());
                $post->setData('modified_date', time());
            }

            $post->setData('title', $title);
            $post->setData('description', $description);

            if (isset($image['delete'])) {
                $post->setData('image', null);
                if (is_file($path, $_FILES['image']['name'])) {
                    unlink($path, $_FILES['image']['name']);
                }
            } else {
                if (!empty($imageName)) {
                    $post->setData('image', $imageName);
                }
            }


            $post->save();
        }  catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            $this->_redirect('*/*');
        }
        $this->_redirect('*/*');

    }

}