Test Project
======================================

1) Create composer.json:
{

    "name": "test/project",
    "type": "project",
    "license": "proprietary",
    "description": "test project ",
    "minimum-stability": "dev",
    "authors": [
        {"name": "A.Kuharenko", "email": "alexei.kukharenko@gmail.com"}
    ],
    "repositories": [

       		{"type": "composer", "url": "https://packages.firegento.com"},
	        {"type": "git", "url": "git@bitbucket.org:kuharenko/alx_blog.git"},

        {
            "type":"package",
            "package":{
                "name":"alx/blog",
                "version":"1.2.3",
                "source":{
                    "type":"git",
                    "url":"https://bitbucket.org/kuharenko/alx_blog",
                    "reference":"master"
                }

            }

        }
    ],
    "require": {
        "magento-hackathon/magento-composer-installer": "*",
        "alx/blog": "dev-master"    
},

    "extra": {
        "magento-deploystrategy" : "symlink",
        "magento-root-dir": "./mageroot",
        "magento-force": "override"   

    }

}

2) run composer install

